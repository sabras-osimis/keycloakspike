import { Component } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import { KeycloakService } from 'keycloak-angular';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  response = '';
  public loggedIn : Promise<Boolean>

  constructor(private http : HttpClient, public kc : KeycloakService){}

  public clear() {
    this.response = '';
  }

  public ngOnInit() {
    this.loggedIn = this.kc.isLoggedIn();
  }

  public sayHello(){
    this.http.get('http://localhost:8080/hello',{ responseType : 'text'})
      .subscribe((response : string) => {
        this.response = response;
      });
  }

  public sayHelloSecurely(){
    this.http.get('http://localhost:8080/secured-hello',{ responseType : 'text'})
      .subscribe(
        (response : string) => {
          this.response = response;
        },
        (error : any) => {
          this.response = error.message;
        });
  }
}
