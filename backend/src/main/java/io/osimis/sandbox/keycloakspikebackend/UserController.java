package io.osimis.sandbox.keycloakspikebackend;

import org.keycloak.KeycloakPrincipal;
import org.keycloak.KeycloakSecurityContext;
import org.keycloak.representations.AccessToken;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {

    @ResponseStatus(value=HttpStatus.NOT_FOUND, reason="No Principal. Is keycloak enabled ?")
    private class NoPrincipalException extends RuntimeException {
        private static final long serialVersionUID = 1L;
    }

    @GetMapping(value="hello", produces="text/plain")
    String hello() {
        return "Hello World";
    }

    @CrossOrigin()
    @GetMapping(value="secured-hello", produces="text/plain")
    String securedHello() {
        return "Hello Secured World";
    }

    @GetMapping("user")
    AccessToken getUser(KeycloakPrincipal<KeycloakSecurityContext> principal) {
        if (principal == null)
            throw new NoPrincipalException();

        return principal.getKeycloakSecurityContext().getToken();
    }
}
