Default admin user
------------------
see section Users/passwords

Setup
-----
The docker-compose file contains the openldap server as well as phpldapadmin for administring the server.
First things to do is to feed the data into ldap. For this, log in as admin, import lify.io.ldif file and you are good to go.

Users/passwords
---------------
| User | Password |
|------|----------|
| cn=admin,dc=lify,dc=io                    | admin     |
| cn=osimis.io,ou=people,dc=lify,dc=io      | 123456    |
| cn=doctor.who,ou=people,dc=lify,dc=io     | 123456    |
| cn=Doctor.Strange,ou=people,dc=lify,dc=io |           |